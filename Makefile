################################################################################
# General build variables.
################################################################################
# What command to use to fetch remote source archives; the command must support
# `-o` to specify the output filename.
FETCH      := fetch
# The make program to be used.
MAKE       := gmake
# How many parallel make jobs to use for individual toolchain builds.
MAKE_JOBS  := `sysctl hw.ncpu | cut -f2 -d' '`
# The GNU source mirror; here for convenience.
GNU_MIRROR := ftp://www.mirrorservice.org/sites/ftp.gnu.org/gnu
# Github base URL.
GH_MIRROR  := https://github.com
# Host triplet for toolchain.
TARGET     := riscv-none-elf

# GCC particulars.
GCC_VERSION := 9.3.0
GCC_CONFIG  := --enable-languages="c" --enable-multilib
GCC_MIRROR  := $(GNU_MIRROR)/gcc/gcc-$(GCC_VERSION)
GCC_DIR     := gcc-$(GCC_VERSION)
GCC_ARCHIVE := $(GCC_DIR).tar.gz

# Binutils particulars.
BINUTILS_VERSION := 2.34
BINUTILS_CONFIG  := --enable-multilib --disable-nls
BINUTILS_MIRROR  := $(GNU_MIRROR)/binutils
BINUTILS_DIR     := binutils-$(BINUTILS_VERSION)
BINUTILS_ARCHIVE := $(BINUTILS_DIR).tar.gz

# Newlib particulars.
NEWLIB_VERSION := 3.3.0
NEWLIB_CONFIG  := --enable-multilib --disable-nls
NEWLIB_MIRROR  := ftp://sourceware.org/pub/newlib
NEWLIB_DIR     := newlib-$(NEWLIB_VERSION)
NEWLIB_ARCHIVE := $(NEWLIB_DIR).tar.gz

# GDB particulars.
GDB_VERSION := 9.1
GDB_CONFIG  := --with-system-zlib --enable-multilib --disable-nls
			   --with-guile=no
GDB_MIRROR  := $(GNU_MIRROR)/gdb
GDB_DIR     := gdb-$(GDB_VERSION)
GDB_ARCHIVE := $(GDB_DIR).tar.gz

# Expat particulars; NOTE: mirror will change with release here.
EXPAT_VERSION := 2.2.9
EXPAT_CONFIG  :=
EXPAT_MIRROR  := $(GH_MIRROR)/libexpat/libexpat/releases/download/R_2_2_9
EXPAT_DIR     := expat-$(EXPAT_VERSION)
EXPAT_ARCHIVE := $(EXPAT_DIR).tar.bz2

# Installation directory.
BASE_DIR := $(TARGET)_$(GCC_VERSION)
PREFIX   := $(HOME)/bin/$(BASE_DIR)

# Build related variables.
ARC_DIR := $(BASE_DIR)/orig
SRC_DIR := $(BASE_DIR)/src
BLD_DIR := $(BASE_DIR)/build

# Ensure cross compiled utilities are in PATH.
PATH := $(PREFIX)/bin:$(PATH)

.DEFAULT_GOAL := all

################################################################################
# General build rules.
################################################################################
.SECONDEXPANSION:
%/.dummy:
	mkdir -p $(dir $@)
	touch $@

################################################################################
# Binutils build rules.
################################################################################
$(ARC_DIR)/$(BINUTILS_ARCHIVE): $$(@D)/.dummy
	$(FETCH) $(BINUTILS_MIRROR)/$(BINUTILS_ARCHIVE) -o $@

$(SRC_DIR)/$(BINUTILS_DIR)/configure: $$(@D)/.dummy $(ARC_DIR)/$(BINUTILS_ARCHIVE)
	tar xzf $(ARC_DIR)/$(BINUTILS_ARCHIVE) -C $(SRC_DIR)

$(BLD_DIR)/$(BINUTILS_DIR): $$@/.dummy $(SRC_DIR)/$(BINUTILS_DIR)/configure
	cd $(BLD_DIR)/$(BINUTILS_DIR) && \
		../../../$(SRC_DIR)/$(BINUTILS_DIR)/configure \
			--prefix=$(PREFIX) \
			--target=$(TARGET) \
			$(BINUTILS_CONFIG)

.PHONY: build_binutils
build_binutils: $(BLD_DIR)/$(BINUTILS_DIR)
	$(MAKE) -C $(BLD_DIR)/$(BINUTILS_DIR) -j $(MAKE_JOBS)

.PHONY: install_binutils
install_binutils: build_binutils
	$(MAKE) -C $(BLD_DIR)/$(BINUTILS_DIR) install

################################################################################
# GCC build rules.
################################################################################
$(ARC_DIR)/$(GCC_ARCHIVE): $$(@D)/.dummy
	$(FETCH) $(GCC_MIRROR)/$(GCC_ARCHIVE) -o $@

$(SRC_DIR)/$(GCC_DIR)/configure: $$(@D)/.dummy $(ARC_DIR)/$(GCC_ARCHIVE)
	tar xzf $(ARC_DIR)/$(GCC_ARCHIVE) -C $(SRC_DIR)
	cd $(SRC_DIR)/$(GCC_DIR) && ./contrib/download_prerequisites

$(BLD_DIR)/$(GCC_DIR): $$@/.dummy $(SRC_DIR)/$(GCC_DIR)/configure
	mkdir -p $(BLD_DIR)/$(GCC_DIR)
	cd $(BLD_DIR)/$(GCC_DIR) && \
		../../../$(SRC_DIR)/$(GCC_DIR)/configure \
			--prefix=$(PREFIX) \
			--target=$(TARGET) \
			--with-newlib \
			--with-headers=../../../$(SRC_DIR)/$(NEWLIB_DIR)/newlib/libc/include \
			$(GCC_CONFIG)

.PHONY: build_gcc
build_gcc: $(BLD_DIR)/$(GCC_DIR)
	$(MAKE) -C $(BLD_DIR)/$(GCC_DIR) -j $(MAKE_JOBS) all-gcc
	$(MAKE) -C $(BLD_DIR)/$(GCC_DIR) -j $(MAKE_JOBS) all-target-libgcc

.PHONY: install_gcc_prelim
install_gcc_prelim: build_gcc
	$(MAKE) -C $(BLD_DIR)/$(GCC_DIR) install-gcc
	$(MAKE) -C $(BLD_DIR)/$(GCC_DIR) install-target-libgcc

.PHONY: install_gcc
install_gcc: build_gcc install_gcc_prelim install_newlib
	$(MAKE) -C $(BLD_DIR)/$(GCC_DIR) all install

################################################################################
# Newlib build rules.
################################################################################
$(ARC_DIR)/$(NEWLIB_ARCHIVE): $$(@D)/.dummy
	$(FETCH) $(NEWLIB_MIRROR)/$(NEWLIB_ARCHIVE) -o $@

$(SRC_DIR)/$(NEWLIB_DIR)/configure: $$(@D)/.dummy $(ARC_DIR)/$(NEWLIB_ARCHIVE)
	tar xzf $(ARC_DIR)/$(NEWLIB_ARCHIVE) -C $(SRC_DIR)

$(BLD_DIR)/$(NEWLIB_DIR): $$@/.dummy $(SRC_DIR)/$(NEWLIB_DIR)/configure
	cd $(BLD_DIR)/$(NEWLIB_DIR) && \
		../../../$(SRC_DIR)/$(NEWLIB_DIR)/configure \
			--prefix=$(PREFIX) \
			--target=$(TARGET) \
			$(NEWLIB_CONFIG)

.PHONY: build_newlib
build_newlib: $(BLD_DIR)/$(NEWLIB_DIR)
	$(MAKE) -C $(BLD_DIR)/$(NEWLIB_DIR) -j $(MAKE_JOBS) all

.PHONY: install_newlib
install_newlib: build_newlib
	$(MAKE) -C $(BLD_DIR)/$(NEWLIB_DIR) install

################################################################################
# Expat build rules.
################################################################################
$(ARC_DIR)/$(EXPAT_ARCHIVE): $$(@D)/.dummy
	$(FETCH) $(EXPAT_MIRROR)/$(EXPAT_ARCHIVE) -o $@

$(SRC_DIR)/$(EXPAT_DIR)/configure: $$(@D)/.dummy $(ARC_DIR)/$(EXPAT_ARCHIVE)
	tar xjf $(ARC_DIR)/$(EXPAT_ARCHIVE) -C $(SRC_DIR)

$(BLD_DIR)/$(EXPAT_DIR): $$@/.dummy $(SRC_DIR)/$(EXPAT_DIR)/configure
	cd $(BLD_DIR)/$(EXPAT_DIR) && \
		../../../$(SRC_DIR)/$(EXPAT_DIR)/configure \
			--prefix=$(PREFIX) \
			--target=$(TARGET) \
			$(EXPAT_CONFIG)

.PHONY: build_expat
build_expat: $(BLD_DIR)/$(EXPAT_DIR)
	$(MAKE) -C $(BLD_DIR)/$(EXPAT_DIR) -j $(MAKE_JOBS) all

.PHONY: install_expat
install_expat: build_expat
	$(MAKE) -C $(BLD_DIR)/$(EXPAT_DIR) install

################################################################################
# GDB build rules.
################################################################################
$(ARC_DIR)/$(GDB_ARCHIVE): $$(@D)/.dummy
	$(FETCH) $(GDB_MIRROR)/$(GDB_ARCHIVE) -o $@

$(SRC_DIR)/$(GDB_DIR)/configure: $$(@D)/.dummy $(ARC_DIR)/$(GDB_ARCHIVE)
	tar xzf $(ARC_DIR)/$(GDB_ARCHIVE) -C $(SRC_DIR)

$(BLD_DIR)/$(GDB_DIR): $$@/.dummy $(SRC_DIR)/$(GDB_DIR)/configure
	cd $(BLD_DIR)/$(GDB_DIR) && \
		../../../$(SRC_DIR)/$(GDB_DIR)/configure \
			--prefix=$(PREFIX) \
			--target=$(TARGET) \
			--with-expat \
			$(GDB_CONFIG)

.PHONY: build_gdb
build_gdb: $(BLD_DIR)/$(GDB_DIR)
	$(MAKE) -C $(BLD_DIR)/$(GDB_DIR) -j $(MAKE_JOBS) \
		LDFLAGS+=-L$(PREFIX)/lib all

.PHONY: install_gdb
install_gdb: install_expat build_gdb
	$(MAKE) -C $(BLD_DIR)/$(GDB_DIR) install

################################################################################
# Sundry clean targets.
################################################################################
.PHONY: clean_build
clean_build:
	rm -rf $(BLD_DIR)

.PHONY: clean_sources
clean_sources:
	rm -rf $(SRC_DIR)

.PHONY: clean_archives
clean_archives:
	rm -rf $(ARC_DIR)

.PHONY: deinstall
deinstall:
	rm -rf $(PREFIX)

.PHONY: clean
clean:
	rm -rf $(BASE_DIR)

all: install_binutils install_gcc install_gdb
	@echo "All done!"

# Prevent _this top level Makefile_ being run in parallel; individual builds
# will be run in parallel.
.NOTPARALLEL:
