# Embedded Toolchain Builder

This is a standalone piece of build infrastructure which will cross build the following components:

 - Binutils
 - GCC with Newlib
 - GDB with Expat

All configuration is done in the single Makefile.

## What will it do?
This is a single Makefile which will cross compile the above components based on user configuration. By default, a directory will be created under `$HOME/bin` whose directory name will be a portmanteau of the host triplet the cross toolchain will targtet and the GCC version: `$HOME/bin/arm-none-eabi_8.3.0`. This can be changed by the user.

## How do I use it?
Once you have configured the parameters e.g. versions, configure script flags, desired command variables and mirrors etc, one simply invokes their system's local GNU `make` program. On non-Linux systems such as BSD, this is `gmake`, or just plain `make` on Linux.

When the build has finished, a cross toolchain will exist in the directory mentioned above. To use it, simply update your `$PATH` environment variable e.g.

```
$ PATH=$HOME/bin/arm-none-eabi_8.3.0/bin:$PATH; export PATH
```
To uninstall the toolchain, invoke the `deinstall` make target; this target will delete the directory refered to by `$PREFIX` i.e. the directory installed into.

## Configuration
At the top of the Makefile, there are several configurable variables affected a number of things e.g. what versions of the tools to fetch, what the cross compilation target is, where the tools should be installed to etc. The comments accompanying these variables should be self explanatory.
